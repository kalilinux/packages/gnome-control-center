#ifndef __RESOURCE_cc_display_H__
#define __RESOURCE_cc_display_H__

#include <gio/gio.h>

extern GResource *cc_display_get_resource (void);
#endif
